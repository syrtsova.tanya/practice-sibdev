const validName = (name) => name.length <= 20 && name.trim().length > 0;

const validImage = (image) => {
  const MAX_IMAGE_SIZE = 1000000;
  if (image) {
    return image.size / MAX_IMAGE_SIZE < 1;
  }
  return false;
};

const validWorkTime = (workTime) => {
  const fromHour = workTime.fromHour.split(':');
  const toHour = workTime.toHour.split(':');

  return fromHour[0] < 24 && fromHour[1] < 60
        && toHour[0] < 24 && toHour[1] < 60
        && workTime.fromHour < workTime.toHour;
};

const getImageSrc = (img) => {
  if (img) {
    return typeof img === 'string' ? img : URL.createObjectURL(img);
  }
  return null;
};

const throwError = (err) => {
  throw err;
};

export {
  validName,
  validImage,
  validWorkTime,
  getImageSrc,
  throwError
};
