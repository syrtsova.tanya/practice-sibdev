import axios from 'axios';

const getIngredientsListService = () => axios.get('http://0.0.0.0:8000/api/ingredients/');

const addIngredientService = (ingredient) => axios({
  method: 'POST',
  headers: {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Token e972cade55252ed3bfa7f6858b738621cdc2600b'
  },
  url: 'http://0.0.0.0:8000/api/ingredients/',
  data: ingredient
});

export {
  getIngredientsListService,
  addIngredientService
};
