import axios from 'axios';

const getDishesListService = (placeId) => axios.get(`http://0.0.0.0:8000/api/dishes/?place=${placeId}`);

const getDishService = (id) => axios.get(`http://0.0.0.0:8000/api/dishes/${id}/`);

const addDishService = (dish) => axios({
  method: 'POST',
  headers: {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Token e972cade55252ed3bfa7f6858b738621cdc2600b'
  },
  url: 'http://0.0.0.0:8000/api/dishes/',
  data: dish
});

const updateDishService = (dishId, dish) => axios({
  method: 'PATCH',
  headers: {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Token e972cade55252ed3bfa7f6858b738621cdc2600b'
  },
  url: `http://0.0.0.0:8000/api/dishes/${dishId}/`,
  data: dish
});

const deleteDishService = (dishId) => axios({
  method: 'DELETE',
  headers: {
    Authorization: 'Token e972cade55252ed3bfa7f6858b738621cdc2600b'
  },
  url: `http://0.0.0.0:8000/api/dishes/${dishId}/`,
});

export {
  getDishesListService,
  getDishService,
  addDishService,
  updateDishService,
  deleteDishService
};
