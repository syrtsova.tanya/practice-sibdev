import axios from 'axios';

const getPlacesListService = () => axios.get('http://0.0.0.0:8000/api/places/');

const getPlaceService = (id) => axios.get(`http://0.0.0.0:8000/api/places/${id}/`);

const addPlaceService = (place) => axios({
  method: 'POST',
  headers: {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Token e972cade55252ed3bfa7f6858b738621cdc2600b'
  },
  url: 'http://0.0.0.0:8000/api/places/',
  data: place
});

const updatePlaceService = (placeId, place) => axios({
  method: 'PATCH',
  headers: {
    'Content-Type': 'multipart/form-data',
    Authorization: 'Token e972cade55252ed3bfa7f6858b738621cdc2600b'
  },
  url: `http://0.0.0.0:8000/api/places/${placeId}/`,
  data: place
});

const deletePlaceService = (placeId) => axios({
  method: 'DELETE',
  headers: {
    Authorization: 'Token e972cade55252ed3bfa7f6858b738621cdc2600b'
  },
  url: `http://0.0.0.0:8000/api/places/${placeId}/`,
});

export {
  getPlacesListService,
  getPlaceService,
  addPlaceService,
  updatePlaceService,
  deletePlaceService
};
