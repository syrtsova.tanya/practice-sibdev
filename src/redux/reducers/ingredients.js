import {
  FETCH_INGREDIENTS_REQUEST,
  FETCH_INGREDIENTS_SUCCESS,
  FETCH_INGREDIENTS_ERROR,
  ADD_INGREDIENT_REQUEST,
  ADD_INGREDIENT_SUCCESS,
  ADD_INGREDIENT_ERROR
} from '../actions/actionTypes';

const initialState = {
  ingredientsList: [],
  error: false,
  loading: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_INGREDIENTS_REQUEST: {
      return {
        ...state,
        loading: true,
        error: false,
        ingredientsList: []
      };
    }
    case FETCH_INGREDIENTS_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: false,
        ingredientsList: action.payload
      };
    }
    case FETCH_INGREDIENTS_ERROR: {
      return {
        ...state,
        loading: false,
        error: true,
        ingredientsList: []
      };
    }
    case ADD_INGREDIENT_REQUEST: {
      return {
        ...state,
        loading: true,
        error: false
      };
    }
    case ADD_INGREDIENT_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: false
      };
    }
    case ADD_INGREDIENT_ERROR: {
      return {
        ...state,
        loading: false,
        error: true
      };
    }
    default:
      return state;
  }
};

export default reducer;
