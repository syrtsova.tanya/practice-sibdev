import { combineReducers } from 'redux';
import place from './place';
import dish from './dish';
import ingredients from './ingredients';

export default combineReducers({
  place,
  dish,
  ingredients
});
