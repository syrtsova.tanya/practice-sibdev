import {
  FETCH_PLACES_REQUEST,
  FETCH_PLACES_SUCCESS,
  FETCH_PLACES_ERROR,
  FETCH_PLACE_REQUEST,
  FETCH_PLACE_SUCCESS,
  FETCH_PLACE_ERROR,
  ADD_PLACE_REQUEST,
  ADD_PLACE_SUCCESS,
  ADD_PLACE_ERROR,
  UPDATE_PLACE_REQUEST,
  UPDATE_PLACE_SUCCESS,
  UPDATE_PLACE_ERROR,
  DELETE_PLACE_REQUEST,
  DELETE_PLACE_SUCCESS,
  DELETE_PLACE_ERROR
} from '../actions/actionTypes';

const initialState = {
  placesList: [],
  editPlace: {},
  loading: false,
  error: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PLACES_REQUEST:
      return {
        ...state,
        loading: true,
        error: false
      };
    case FETCH_PLACES_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        placesList: action.payload
      };
    case FETCH_PLACES_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        placesList: []
      };
    case FETCH_PLACE_REQUEST:
      return {
        ...state,
        loading: true,
        error: false,
        editPlace: {}
      };
    case FETCH_PLACE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        editPlace: action.payload
      };
    case FETCH_PLACE_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        editPlace: {}
      };
    case ADD_PLACE_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ADD_PLACE_ERROR:
      return {
        ...state,
        loading: false,
        error: true
      };
    case ADD_PLACE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false
      };
    case UPDATE_PLACE_REQUEST:
      return {
        ...state,
        loading: true,
        error: false
      };
    case UPDATE_PLACE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false
      };
    case UPDATE_PLACE_ERROR:
      return {
        ...state,
        loading: false,
        error: true
      };
    case DELETE_PLACE_REQUEST:
      return {
        ...state,
        loading: true,
        error: false
      };
    case DELETE_PLACE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false
      };
    case DELETE_PLACE_ERROR:
      return {
        ...state,
        loading: false,
        error: true
      };
    default:
      return state;
  }
};

export default reducer;
