import {
  FETCH_DISHES_REQUEST,
  FETCH_DISHES_SUCCESS,
  FETCH_DISHES_ERROR,
  FETCH_DISH_REQUEST,
  FETCH_DISH_SUCCESS,
  FETCH_DISH_ERROR,
  ADD_DISH_REQUEST,
  ADD_DISH_SUCCESS,
  ADD_DISH_ERROR,
  UPDATE_DISH_REQUEST,
  UPDATE_DISH_SUCCESS,
  UPDATE_DISH_ERROR,
  DELETE_DISH_REQUEST,
  DELETE_DISH_SUCCESS,
  DELETE_DISH_ERROR
} from '../actions/actionTypes';

const initialState = {
  dishesList: [],
  editDish: {},
  error: false,
  loading: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_DISHES_REQUEST:
      return {
        ...state,
        loading: true,
        error: false
      };
    case FETCH_DISHES_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        dishesList: action.payload
      };
    case FETCH_DISHES_ERROR:
      return {
        ...state,
        loading: false,
        error: true
      };
    case FETCH_DISH_REQUEST:
      return {
        ...state,
        loading: true,
        editDish: {}
      };
    case FETCH_DISH_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        editDish: action.payload
      };
    case FETCH_DISH_ERROR:
      return {
        ...state,
        loading: false,
        error: true
      };
    case ADD_DISH_REQUEST:
      return {
        ...state,
        loading: true,
        error: false
      };
    case ADD_DISH_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false
      };
    case ADD_DISH_ERROR:
      return {
        ...state,
        loading: false,
        error: true
      };
    case UPDATE_DISH_REQUEST:
      return {
        ...state,
        loading: true,
        error: false
      };
    case UPDATE_DISH_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false
      };
    case UPDATE_DISH_ERROR:
      return {
        ...state,
        loading: false,
        error: true
      };
    case DELETE_DISH_REQUEST:
      return {
        ...state,
        loading: true,
        error: false
      };
    case DELETE_DISH_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false
      };
    case DELETE_DISH_ERROR:
      return {
        ...state,
        loading: false,
        error: true
      };
    default:
      return state;
  }
};

export default reducer;
