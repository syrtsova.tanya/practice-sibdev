import { throwError } from '../../utils/utils';
import {
  getDishesListService,
  getDishService,
  addDishService,
  updateDishService,
  deleteDishService
} from '../../services/dishesServices';

import {
  FETCH_DISHES_REQUEST,
  FETCH_DISHES_SUCCESS,
  FETCH_DISHES_ERROR,
  FETCH_DISH_REQUEST,
  FETCH_DISH_SUCCESS,
  FETCH_DISH_ERROR,
  ADD_DISH_REQUEST,
  ADD_DISH_SUCCESS,
  ADD_DISH_ERROR,
  UPDATE_DISH_REQUEST,
  UPDATE_DISH_SUCCESS,
  UPDATE_DISH_ERROR,
  DELETE_DISH_REQUEST,
  DELETE_DISH_SUCCESS,
  DELETE_DISH_ERROR
} from './actionTypes';

function fetchDishes(placeId) {
  function fetchDishesRequested() {
    return {
      type: FETCH_DISHES_REQUEST
    };
  }

  function fetchDishesSuccess(newDishes) {
    return {
      type: FETCH_DISHES_SUCCESS,
      payload: newDishes
    };
  }

  function fetchDishesError() {
    return {
      type: FETCH_DISHES_ERROR
    };
  }

  return (dispatch) => {
    dispatch(fetchDishesRequested());
    return getDishesListService(placeId)
      .then((results) => dispatch(fetchDishesSuccess(results.data)))
      .catch((err) => {
        dispatch(fetchDishesError());
        throwError(err);
      });
  };
}

function fetchDish(dishId) {
  function fetchDishRequested() {
    return {
      type: FETCH_DISH_REQUEST
    };
  }

  function fetchDishSuccess(newDishes) {
    return {
      type: FETCH_DISH_SUCCESS,
      payload: newDishes
    };
  }

  function fetchDishError() {
    return {
      type: FETCH_DISH_ERROR
    };
  }

  return (dispatch) => {
    dispatch(fetchDishRequested());
    return getDishService(dishId)
      .then((results) => dispatch(fetchDishSuccess(results.data)))
      .catch((err) => {
        dispatch(fetchDishError());
        throwError(err);
      });
  };
}

function addDish(dish) {
  function addDishRequested() {
    return {
      type: ADD_DISH_REQUEST
    };
  }

  function addDishSuccess() {
    return {
      type: ADD_DISH_SUCCESS
    };
  }

  function addDishError() {
    return {
      type: ADD_DISH_ERROR
    };
  }

  return (dispatch) => {
    dispatch(addDishRequested());
    return addDishService(dish)
      .then(() => dispatch(addDishSuccess()))
      .catch((err) => {
        dispatch(addDishError());
        throwError(err);
      });
  };
}

function updateDish(dishId, dish) {
  function updateDishRequested() {
    return {
      type: UPDATE_DISH_REQUEST
    };
  }

  function updateDishSuccess() {
    return {
      type: UPDATE_DISH_SUCCESS
    };
  }

  function updateDishError() {
    return {
      type: UPDATE_DISH_ERROR
    };
  }

  return (dispatch) => {
    dispatch(updateDishRequested());
    return updateDishService(dishId, dish)
      .then(() => dispatch(updateDishSuccess()))
      .catch((err) => {
        dispatch(updateDishError());
        throwError(err);
      });
  };
}

function deleteDish(dishId) {
  function deleteDishRequested() {
    return {
      type: DELETE_DISH_REQUEST
    };
  }

  function deleteDishSuccess() {
    return {
      type: DELETE_DISH_SUCCESS
    };
  }

  function deleteDishError() {
    return {
      type: DELETE_DISH_ERROR
    };
  }

  return (dispatch) => {
    dispatch(deleteDishRequested());
    return deleteDishService(dishId)
      .then(() => dispatch(deleteDishSuccess()))
      .catch((err) => {
        dispatch(deleteDishError());
        throwError(err);
      });
  };
}

export {
  fetchDishes,
  fetchDish,
  addDish,
  updateDish,
  deleteDish
};
