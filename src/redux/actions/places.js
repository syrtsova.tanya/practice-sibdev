import { throwError } from '../../utils/utils';
import {
  getPlacesListService,
  getPlaceService,
  addPlaceService,
  updatePlaceService,
  deletePlaceService
} from '../../services/placeServices';

import {
  FETCH_PLACES_REQUEST,
  FETCH_PLACES_SUCCESS,
  FETCH_PLACES_ERROR,
  FETCH_PLACE_REQUEST,
  FETCH_PLACE_SUCCESS,
  FETCH_PLACE_ERROR,
  ADD_PLACE_REQUEST,
  ADD_PLACE_SUCCESS,
  ADD_PLACE_ERROR,
  UPDATE_PLACE_REQUEST,
  UPDATE_PLACE_SUCCESS,
  UPDATE_PLACE_ERROR,
  DELETE_PLACE_REQUEST,
  DELETE_PLACE_SUCCESS,
  DELETE_PLACE_ERROR
} from './actionTypes';

function fetchPlaces() {
  function fetchPlacesRequested() {
    return {
      type: FETCH_PLACES_REQUEST
    };
  }

  function fetchPlacesLoaded(newPlaces) {
    return {
      type: FETCH_PLACES_SUCCESS,
      payload: newPlaces
    };
  }

  function fetchPlacesError() {
    return {
      type: FETCH_PLACES_ERROR
    };
  }

  return (dispatch) => {
    dispatch(fetchPlacesRequested());
    return getPlacesListService()
      .then((results) => dispatch(fetchPlacesLoaded(results.data)))
      .catch((err) => {
        dispatch(fetchPlacesError());
        throwError(err);
      });
  };
}

function fetchPlace(id) {
  function fetchPlaceRequested() {
    return {
      type: FETCH_PLACE_REQUEST
    };
  }

  function fetchPlaceLoaded(newPlace) {
    return {
      type: FETCH_PLACE_SUCCESS,
      payload: newPlace
    };
  }

  function fetchPlaceError() {
    return {
      type: FETCH_PLACE_ERROR
    };
  }

  return (dispatch) => {
    dispatch(fetchPlaceRequested());
    return getPlaceService(id)
      .then((results) => dispatch(fetchPlaceLoaded(results.data)))
      .catch((err) => {
        dispatch(fetchPlaceError());
        throwError(err);
      });
  };
}

function addPlace(place) {
  function addPlaceRequested() {
    return {
      type: ADD_PLACE_REQUEST
    };
  }

  function addPlaceLoaded(newPlace) {
    return {
      type: ADD_PLACE_SUCCESS,
      payload: newPlace
    };
  }

  function addPlaceError() {
    return {
      type: ADD_PLACE_ERROR
    };
  }

  return (dispatch) => {
    dispatch(addPlaceRequested());
    return addPlaceService(place)
      .then((results) => dispatch(addPlaceLoaded(results.data)))
      .catch((err) => {
        dispatch(addPlaceError());
        throwError(err);
      });
  };
}

function updatePlace(placeId, place) {
  function updatePlaceRequested() {
    return {
      type: UPDATE_PLACE_REQUEST
    };
  }

  function updatePlaceSuccess() {
    return {
      type: UPDATE_PLACE_SUCCESS
    };
  }

  function updatePlaceError() {
    return {
      type: UPDATE_PLACE_ERROR
    };
  }

  return (dispatch) => {
    dispatch(updatePlaceRequested());
    return updatePlaceService(placeId, place)
      .then(() => dispatch(updatePlaceSuccess()))
      .catch((err) => {
        dispatch(updatePlaceError());
        throwError(err);
      });
  };
}

function deletePlaceAction(placeId) {
  function deletePlaceRequested() {
    return {
      type: DELETE_PLACE_REQUEST
    };
  }

  function deletePlaceSuccess() {
    return {
      type: DELETE_PLACE_SUCCESS
    };
  }

  function deletePlaceError() {
    return {
      type: DELETE_PLACE_ERROR
    };
  }

  return (dispatch) => {
    dispatch(deletePlaceRequested());
    return deletePlaceService(placeId)
      .then(() => {
        dispatch(deletePlaceSuccess());
      })
      .catch((err) => {
        dispatch(deletePlaceError());
        throwError(err);
      });
  };
}

export {
  fetchPlaces,
  fetchPlace,
  addPlace,
  updatePlace,
  deletePlaceAction
};
