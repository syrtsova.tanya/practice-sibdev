import { throwError } from '../../utils/utils';
import {
  getIngredientsListService, addIngredientService
} from '../../services/ingredientServices';

import {
  FETCH_INGREDIENTS_REQUEST,
  FETCH_INGREDIENTS_SUCCESS,
  FETCH_INGREDIENTS_ERROR,
  ADD_INGREDIENT_REQUEST,
  ADD_INGREDIENT_SUCCESS,
  ADD_INGREDIENT_ERROR
} from './actionTypes';

function fetchIngredients() {
  function fetchIngredientsRequested() {
    return {
      type: FETCH_INGREDIENTS_REQUEST
    };
  }

  function fetchIngredientsSuccess(newIngredients) {
    return {
      type: FETCH_INGREDIENTS_SUCCESS,
      payload: newIngredients
    };
  }

  function fetchIngredientsError() {
    return {
      type: FETCH_INGREDIENTS_ERROR
    };
  }

  return (dispatch) => {
    dispatch(fetchIngredientsRequested());
    return getIngredientsListService()
      .then((res) => dispatch(fetchIngredientsSuccess(res.data)))
      .catch((err) => {
        dispatch(fetchIngredientsError());
        throwError(err);
      });
  };
}

function addIngredient(ingredient) {
  function addIngredientRequest() {
    return {
      type: ADD_INGREDIENT_REQUEST
    };
  }

  function addIngredientSuccess() {
    return {
      type: ADD_INGREDIENT_SUCCESS
    };
  }

  function addIngredientError() {
    return {
      type: ADD_INGREDIENT_ERROR
    };
  }

  return (dispatch) => {
    dispatch(addIngredientRequest());
    return addIngredientService(ingredient)
      .then(() => dispatch(addIngredientSuccess()))
      .catch((err) => {
        dispatch(addIngredientError());
        throwError(err);
      });
  };
}

export {
  fetchIngredients,
  addIngredient
};
