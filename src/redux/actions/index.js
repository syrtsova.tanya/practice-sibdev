import {
  fetchPlaces,
  fetchPlace,
  addPlace,
  updatePlace,
  deletePlaceAction
} from './places';

import {
  fetchDishes,
  fetchDish,
  addDish,
  updateDish,
  deleteDish
} from './dishes';

import {
  fetchIngredients,
  addIngredient
} from './ingredients';

export {
  fetchPlaces,
  fetchPlace,
  addPlace,
  updatePlace,
  deletePlaceAction,
  fetchDishes,
  fetchDish,
  addDish,
  updateDish,
  deleteDish,
  fetchIngredients,
  addIngredient
};
