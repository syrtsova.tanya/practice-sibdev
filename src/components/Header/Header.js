import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  Menu, Container, Segment, Button
} from 'semantic-ui-react';

export default class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeItem: ''
    };
  }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  render() {
    const { activeItem } = this.state;
    return (
      <Segment>
        <Menu secondary stackable>
          <Container>
            <Link to="/">
              <Menu.Item as="span" name="home" active={activeItem === 'home'} onClick={this.handleItemClick}>
                Главная
              </Menu.Item>
            </Link>
            <Link to="/owner/places">
              <Menu.Item as="span" name="owner-places" active={activeItem === 'owner-places'} onClick={this.handleItemClick}>
                Мои заведения
              </Menu.Item>
            </Link>
            <Menu.Menu position="right">
              <Menu.Item>Вы вошли как user49024</Menu.Item>
              <Link to="/">
                <Menu.Item as="span" name="sign-in" onClick={this.handleItemClick}>
                  <Button color="teal">
                    Выйти
                  </Button>
                </Menu.Item>
              </Link>
            </Menu.Menu>
          </Container>
        </Menu>
      </Segment>
    );
  }
}
