import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { Button, Header, Icon } from 'semantic-ui-react';

import './modal.scss';

const modalRoot = document.getElementById('root-modal');

class Modal extends Component {
  render() {
    const { children, onCloseModal, modalTitle } = this.props;
    return ReactDOM.createPortal(
      <div className="modal">
        <div className="modal__wrapper" />
        <div className="modal__content">
          <div className="modal__header">
            <Header>{modalTitle}</Header>
            <Button basic icon onClick={onCloseModal}>
              <Icon name="close" />
            </Button>
          </div>
          {children}
        </div>
      </div>,
      modalRoot,
    );
  }
}

Modal.propTypes = {
  children: PropTypes.node.isRequired,
  onCloseModal: PropTypes.func.isRequired,
  modalTitle: PropTypes.string.isRequired
};

export default Modal;
