import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { Container } from 'semantic-ui-react';

import Header from '../Header';
import HomePage from '../../view/HomePage';
import EditPlacePage from '../../view/EditPlacePage';
import EditDishPage from '../../view/EditDishPage';
import OwnerPlacesPage from '../../view/OwnerPlacesPage';

function Router() {
  return (
    <BrowserRouter>
      <Header />
      <Container>
        <Route path="/" exact component={HomePage} />
        <Route path="/owner/places" exact component={OwnerPlacesPage} />
        <Route
          path="/owner/new-place"
          exact
          render={() => <EditPlacePage />}
        />
        <Route
          path="/owner/places/:id"
          exact
          render={({ match }) => {
            const { id } = match.params;
            return <EditPlacePage placeId={id} />;
          }}
        />
        <Route
          path="/owner/places/:id/dishes/:dishId"
          exact
          render={({ match }) => {
            const { id, dishId } = match.params;
            return <EditDishPage placeId={id} dishId={dishId} />;
          }}
        />
      </Container>
    </BrowserRouter>
  );
}

export default Router;
