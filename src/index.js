import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import 'semantic-ui-css/semantic.min.css';

import store from './redux/store';
import Router from './components/Router';

render(
  <Provider store={store}>
    <Router />
  </Provider>,
  document.getElementById('root')
);
