import React from 'react';
import { Header } from 'semantic-ui-react';

function HomePage() {
  return <Header as="h2">Главная</Header>;
}

export default HomePage;
