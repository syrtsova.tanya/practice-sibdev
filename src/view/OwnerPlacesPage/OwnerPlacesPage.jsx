import React from 'react';
import { Link } from 'react-router-dom';
import {
  Header, Container, Card, Button
} from 'semantic-ui-react';

import PlacesList from './components/PlacesList';

function OwnerPlacesPage() {
  return (
    <Container fluid>
      <Header as="h2"> Мои заведения</Header>
      <Card>
        <Card.Content>
          <PlacesList />
        </Card.Content>
        <Card.Content>
          <Link to="/owner/new-place">
            <Button fluid color="teal">
              Добавить заведение
            </Button>
          </Link>
        </Card.Content>
      </Card>
    </Container>
  );
}

export default OwnerPlacesPage;
