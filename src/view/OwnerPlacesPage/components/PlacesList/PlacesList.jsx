import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { List } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import { fetchPlaces } from '../../../../redux/actions';
import './placesList.scss';

class PlacesList extends Component {
  componentDidMount() {
    const { getPlacesList } = this.props;
    getPlacesList();
  }

  render() {
    const { placesList, loading } = this.props;

    if (loading) {
      return <div>loading...</div>;
    }
    return (
      <List className="places-list">
        {
          placesList.map((place) => (
            <Link key={place.id} to={`/owner/places/${place.id}`}>
              <List.Item className="places-list__item">
                {place.name}
              </List.Item>
            </Link>
          ))
        }
      </List>
    );
  }
}

const mapStateToProps = (state) => ({
  placesList: state.place.placesList,
  loading: state.place.loading
});

const mapDispatchToProps = {
  getPlacesList: fetchPlaces
};

PlacesList.propTypes = {
  getPlacesList: PropTypes.func.isRequired,
  placesList: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string
  })),
  loading: PropTypes.bool.isRequired
};

PlacesList.defaultProps = {
  placesList: []
};

export default connect(mapStateToProps, mapDispatchToProps)(PlacesList);
