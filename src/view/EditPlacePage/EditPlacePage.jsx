import React from 'react';
import { Container } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import EditPlace from './components/EditPlace';

function EditPlacePage(props) {
  const { placeId } = props;
  return (
    <Container>
      <EditPlace placeId={placeId} />
    </Container>
  );
}

EditPlacePage.propTypes = {
  placeId: PropTypes.string
};

EditPlacePage.defaultProps = {
  placeId: ''
};

export default EditPlacePage;
