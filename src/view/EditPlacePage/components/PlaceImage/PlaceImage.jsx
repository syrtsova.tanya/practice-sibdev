import React from 'react';
import { Form, Image, Input } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import { getImageSrc } from '../../../../utils/utils';

function PlaceImage(props) {
  const { placeImage, onChangeImage, isImageValid } = props;

  return (
    <Form.Group inline className="edit-place__form-item">
      {!isImageValid && <span className="error-label">Ошибка!</span>}
      <div className="form-label">
        <Input className="form-file" type="file" name="placeImage" accept="image/jpeg,image/png,image/gif" onChange={onChangeImage} />
      </div>
      <div className="form-input">
        <Image src={getImageSrc(placeImage)} size="medium" />
      </div>
    </Form.Group>
  );
}

PlaceImage.propTypes = {
  placeImage: PropTypes.string,
  onChangeImage: PropTypes.func.isRequired,
  isImageValid: PropTypes.bool.isRequired
};

PlaceImage.defaultProps = {
  placeImage: ''
};

export default PlaceImage;
