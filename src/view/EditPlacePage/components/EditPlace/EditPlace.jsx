import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Header, Segment, Button, Form, Grid
} from 'semantic-ui-react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import {
  fetchPlace, addPlace, updatePlace, deletePlaceAction
} from '../../../../redux/actions';
import {
  validName, validImage, validWorkTime, getImageSrc
} from '../../../../utils/utils';

import PlaceName from '../PlaceName';
import PlaceWorkTime from '../PlaceWorkTime';
import PlaceAddress from '../PlaceAddress';
import PlaceDishes from '../PlaceDishes';
import PlaceImage from '../PlaceImage';

import './editPlace.scss';

class EditPlace extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      image: null,
      fromHour: '',
      toHour: '',
      address: '',
      formValid: {
        nameValid: true,
        workTimeValid: true,
        imageValid: true
      }
    };
  }

  componentDidMount() {
    const { getEditPlace, placeId } = this.props;
    if (placeId) {
      getEditPlace(placeId);
    }
  }

  componentDidUpdate(prevProps) {
    const { editPlace } = this.props;
    if (editPlace) {
      this.updateEditPlace(prevProps.editPlace, editPlace);
    }
  }

  updateEditPlace = (prevEditPlace, nextEditPlace) => {
    if (nextEditPlace !== prevEditPlace) {
      this.setState({
        name: nextEditPlace.name,
        image: nextEditPlace.image,
        fromHour: nextEditPlace.from_hour,
        toHour: nextEditPlace.to_hour,
        address: nextEditPlace.address
      });
    }
  }

  handleChangeInput = (e, key) => {
    const inputValue = e.target.value;
    this.setState({
      [key]: inputValue
    });
  }

  handleChangeImage = (e) => {
    const image = e.target.files[0];
    if (image) {
      this.setState({ image });
    }
  }

  getUpdatedFields = (newPlace, editPlace) => {
    const updatedFields = {};
    const allFields = Object.keys(newPlace);
    allFields.forEach((key) => {
      if (newPlace[key] !== editPlace[key]) {
        updatedFields[key] = newPlace[key];
      }
    });

    return updatedFields;
  }

  getFormData = (place) => {
    const formData = new FormData();
    if (place.name) {
      formData.append('name', place.name);
    }
    if (place.address) {
      formData.append('address', place.address);
    }
    if (place.fromHour) {
      formData.append('from_hour', place.fromHour);
    }
    if (place.toHour) {
      formData.append('to_hour', place.toHour);
    }
    if (place.image) {
      formData.append('image', place.image);
    }
    return formData;
  }

  handleUpdatePlace = () => {
    const {
      name, image, fromHour, toHour, address
    } = this.state;
    const {
      placeId, updateEditPlace, editPlace, history
    } = this.props;
    const newPlace = {
      name,
      image,
      fromHour,
      toHour,
      address
    };

    const updatedPlace = this.getUpdatedFields(newPlace, editPlace);

    const isValidName = typeof updatedPlace.name !== 'undefined' ? validName(newPlace.name) : true;
    const isValidImage = updatedPlace.image ? validImage(newPlace.image) : true;

    const isValidWorkTime = typeof updatedPlace.fromHour !== 'undefined' || typeof updatedPlace.toHour !== 'undefined'
      ? validWorkTime({ fromHour: newPlace.fromHour, toHour: newPlace.toHour })
      : true;

    if (isValidName && isValidImage && isValidWorkTime) {
      const formData = this.getFormData(updatedPlace);
      updateEditPlace(placeId, formData)
        .then(() => {
          history.push('/owner/places');
        });
    } else {
      this.setState({
        formValid: {
          nameValid: isValidName,
          workTimeValid: isValidWorkTime,
          imageValid: isValidImage
        }
      });
    }
  }

  handleAddPlace = () => {
    const {
      name, image, fromHour, toHour, address
    } = this.state;
    const { addNewPlace, history } = this.props;
    const newPlace = {
      name, image, fromHour, toHour, address
    };

    const isValidName = validName(newPlace.name);
    const isValidImage = validImage(newPlace.image);
    const isValidWorkTime = validWorkTime({ fromHour: newPlace.fromHour, toHour: newPlace.toHour });

    if (isValidName && isValidImage && isValidWorkTime) {
      addNewPlace(this.getFormData(newPlace))
        .then(() => {
          history.push('/owner/places');
        });
    } else {
      this.setState({
        formValid: {
          nameValid: isValidName,
          workTimeValid: isValidWorkTime,
          imageValid: isValidImage
        }
      });
    }
  }

  handleDeletePlace = () => {
    const { placeId, deletePlace, history } = this.props;
    deletePlace(placeId)
      .then(() => {
        history.push('/owner/places');
      });
  }

  render() {
    const {
      name, image, fromHour, toHour, address, formValid
    } = this.state;
    const { loading, error, placeId } = this.props;

    if (loading) {
      return <div>loading...</div>;
    }

    if (error) {
      return <div>Error!</div>;
    }

    return (
      <Segment>
        <Header>{!placeId ? 'Создать заведение' : 'Редактировать заведение'}</Header>
        <Form>
          <Grid columns="two">
            <Grid.Row>
              <Grid.Column>
                <PlaceName
                  isNameValid={formValid.nameValid}
                  placeName={name}
                  onChangeName={this.handleChangeInput}
                />
                <PlaceWorkTime
                  fromHour={fromHour}
                  toHour={toHour}
                  isWorkTimeValid={formValid.workTimeValid}
                  onChangeFromHour={this.handleChangeInput}
                  onChangeToHour={this.handleChangeInput}
                />
                <PlaceAddress
                  placeAddress={address}
                  onChangeAddress={this.handleChangeInput}
                />
                <PlaceImage
                  isImageValid={formValid.imageValid}
                  placeImage={getImageSrc(image)}
                  onChangeImage={this.handleChangeImage}
                />
              </Grid.Column>
              <Grid.Column>
                <PlaceDishes placeId={placeId} />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                {
                  !placeId
                    ? (
                      <Form.Group>
                        <Form.Field control={Button} color="teal" onClick={this.handleAddPlace}>Добавить</Form.Field>
                      </Form.Group>
                    )
                    : (
                      <Form.Group>
                        <Form.Field control={Button} color="teal" onClick={this.handleUpdatePlace}>Сохранить</Form.Field>
                        <Form.Field control={Button} basic color="teal" onClick={this.handleDeletePlace}>Удалить</Form.Field>
                      </Form.Group>
                    )
                }
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Form>
      </Segment>
    );
  }
}

const mapStateToProps = (state) => ({
  editPlace: state.place.editPlace,
  loading: state.place.loading,
  error: state.place.error
});

const mapDispatchToProps = {
  getEditPlace: fetchPlace,
  addNewPlace: addPlace,
  updateEditPlace: updatePlace,
  deletePlace: deletePlaceAction
};

EditPlace.propTypes = {
  loading: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
  placeId: PropTypes.string,
  editPlace: PropTypes.shape({
    name: PropTypes.string,
    image: PropTypes.string,
    fromHour: PropTypes.string,
    toHour: PropTypes.string,
    address: PropTypes.string
  }),
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  getEditPlace: PropTypes.func.isRequired,
  addNewPlace: PropTypes.func.isRequired,
  updateEditPlace: PropTypes.func.isRequired,
  deletePlace: PropTypes.func.isRequired
};

EditPlace.defaultProps = {
  editPlace: {
    name: '',
    image: null,
    fromHour: '',
    toHour: '',
    address: ''
  },
  placeId: ''
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(EditPlace));
