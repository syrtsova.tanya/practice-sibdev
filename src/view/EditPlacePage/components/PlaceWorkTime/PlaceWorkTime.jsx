import React from 'react';
import { Form } from 'semantic-ui-react';
import MaskedInput from 'react-text-mask';
import PropTypes from 'prop-types';

function PlaceWorkTime(props) {
  const {
    fromHour, toHour, onChangeFromHour, onChangeToHour, isWorkTimeValid
  } = props;
  return (
    <Form.Group inline className="edit-place__form-item">
      {!isWorkTimeValid && <span className="error-label">Ошибка!</span>}
      <div className="form-label">
        <label htmlFor="fromHour">Режим работы</label>
      </div>
      <div className="form-input">
        <div className="ui input">
          <MaskedInput
            mask={[/\d/, /\d/, ':', /\d/, /\d/]}
            placeholder="09:00"
            value={fromHour}
            onChange={(e) => { onChangeFromHour(e, 'fromHour'); }}
            id="fromHour"
          />
        </div>
        <div className="ui input">
          <MaskedInput
            className="ui input"
            mask={[/\d/, /\d/, ':', /\d/, /\d/]}
            placeholder="23:00"
            value={toHour}
            onChange={(e) => { onChangeToHour(e, 'toHour'); }}
            id="toHour"
          />
        </div>
      </div>
    </Form.Group>
  );
}

PlaceWorkTime.defaultProps = {
  fromHour: '',
  toHour: ''
};

PlaceWorkTime.propTypes = {
  fromHour: PropTypes.string,
  toHour: PropTypes.string,
  onChangeFromHour: PropTypes.func.isRequired,
  onChangeToHour: PropTypes.func.isRequired,
  isWorkTimeValid: PropTypes.bool.isRequired
};

PlaceWorkTime.defaultProps = {
  fromHour: '',
  toHour: ''
};

export default PlaceWorkTime;
