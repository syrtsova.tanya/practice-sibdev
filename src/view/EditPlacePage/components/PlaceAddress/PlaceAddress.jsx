import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import PropTypes from 'prop-types';

function PlaceAddress(props) {
  const { placeAddress, onChangeAddress } = props;
  return (
    <Form.Group inline>
      <div className="form-label">
        <label htmlFor="placeAddress">Адрес</label>
      </div>
      <div className="form-input">
        <Input id="placeAddress" value={placeAddress} onChange={(e) => { onChangeAddress(e, 'address'); }} />
      </div>
    </Form.Group>
  );
}

PlaceAddress.propTypes = {
  placeAddress: PropTypes.string,
  onChangeAddress: PropTypes.func.isRequired
};

PlaceAddress.defaultProps = {
  placeAddress: ''
};

export default PlaceAddress;
