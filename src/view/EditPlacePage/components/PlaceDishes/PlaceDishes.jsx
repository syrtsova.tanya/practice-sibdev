import React from 'react';
import { Card, Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import DishesList from '../DishesList';

function PlaceDishes(props) {
  const { placeId } = props;
  return (
    <Card>
      <Card.Content>
        <Card.Header>Блюда</Card.Header>
        <DishesList placeId={placeId} />
      </Card.Content>
      <Card.Content>
        <Link to={`/owner/places/${placeId}/dishes/new`}>
          <Button fluid basic color="teal">
            Добавить блюдо
          </Button>
        </Link>
      </Card.Content>
    </Card>
  );
}

PlaceDishes.propTypes = {
  placeId: PropTypes.string.isRequired
};

export default PlaceDishes;
