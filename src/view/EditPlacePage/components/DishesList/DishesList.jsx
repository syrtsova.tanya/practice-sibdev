import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { List } from 'semantic-ui-react';
import { PropTypes } from 'prop-types';

import { fetchDishes } from '../../../../redux/actions';

class DishesList extends Component {
  componentDidMount() {
    const { getDishesList, placeId } = this.props;
    getDishesList(placeId);
  }

  render() {
    const {
      dishesList, loading, error, placeId
    } = this.props;

    if (loading) {
      return <div>loading...</div>;
    }

    if (error) {
      return <div>error</div>;
    }
    return (
      <List>
        {
          dishesList.map((dish) => (
            <Link key={dish.id} to={`/owner/places/${placeId}/dishes/${dish.id}`}>
              <List.Item>
                {dish.name}
              </List.Item>
            </Link>
          ))
        }
      </List>
    );
  }
}

const mapStateToProps = (state) => ({
  dishesList: state.dish.dishesList,
  loading: state.dish.loading,
  error: state.dish.error
});

const mapDispatchToProps = {
  getDishesList: fetchDishes
};

DishesList.propTypes = {
  placeId: PropTypes.string.isRequired,
  getDishesList: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
  dishesList: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string
  }))
};

DishesList.defaultProps = {
  dishesList: []
};

export default connect(mapStateToProps, mapDispatchToProps)(DishesList);
