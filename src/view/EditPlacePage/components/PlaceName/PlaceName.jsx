import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import PropTypes from 'prop-types';

function PlaceName(props) {
  const { placeName, isNameValid, onChangeName } = props;
  return (
    <Form.Group inline className="edit-place__form-item">
      {!isNameValid && <span className="error-label">Ошибка!</span>}
      <div className="form-label">
        <label htmlFor="placeName">Название</label>
      </div>
      <div className="form-input">
        <Input id="placeName" value={placeName} onChange={(e) => { onChangeName(e, 'name'); }} />
      </div>
    </Form.Group>
  );
}

PlaceName.propTypes = {
  placeName: PropTypes.string,
  isNameValid: PropTypes.bool.isRequired,
  onChangeName: PropTypes.func.isRequired
};

PlaceName.defaultProps = {
  placeName: ''
};

export default PlaceName;
