import React from 'react';
import { Form, Image, Input } from 'semantic-ui-react';
import PropTypes from 'prop-types';

function DishImage(props) {
  const { dishImage, onChangeImage } = props;

  return (
    <Form.Group inline>
      <div className="form-label">
        <Input className="form-file" type="file" name="file" accept="image/jpeg,image/png,image/gif" onChange={onChangeImage} />
      </div>
      <div className="form-input">
        <Image src={dishImage} size="medium" />
      </div>
    </Form.Group>
  );
}

DishImage.propTypes = {
  dishImage: PropTypes.string,
  onChangeImage: PropTypes.func.isRequired
};

DishImage.defaultProps = {
  dishImage: ''
};

export default DishImage;
