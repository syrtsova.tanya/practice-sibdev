import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import PropTypes from 'prop-types';

function DishPrice(props) {
  const { dishPrice, onChangePrice } = props;
  return (
    <Form.Group inline>
      <div className="form-label">
        <label htmlFor="dishPrice">Цена</label>
      </div>
      <div className="form-input">
        <Input id="dishPrice" type="number" value={dishPrice} onChange={(e) => { onChangePrice(e, 'price'); }} />
      </div>
    </Form.Group>
  );
}

DishPrice.propTypes = {
  dishPrice: PropTypes.number,
  onChangePrice: PropTypes.func.isRequired
};

DishPrice.defaultProps = {
  dishPrice: 0
};

export default DishPrice;
