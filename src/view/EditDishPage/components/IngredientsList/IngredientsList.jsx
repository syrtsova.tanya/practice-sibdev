import React from 'react';
import { Card, Button, List } from 'semantic-ui-react';
import PropTypes from 'prop-types';

function IngredientsList(props) {
  const { ingredients, onShowModal } = props;
  return (
    <Card>
      <Card.Content>
        <Card.Header>Ингредиенты</Card.Header>
        <List>
          {
            ingredients && ingredients.map((ingredient) => (
              <List.Item key={ingredient.id}>
                {ingredient.name}
              </List.Item>
            ))
          }
        </List>
      </Card.Content>
      <Card.Content>
        <Button fluid basic color="teal" onClick={onShowModal}>
          Изменить список
        </Button>
      </Card.Content>
    </Card>
  );
}

IngredientsList.propTypes = {
  ingredients: PropTypes.arrayOf(PropTypes.object),
  onShowModal: PropTypes.func.isRequired
};

IngredientsList.defaultProps = {
  ingredients: []
};

export default IngredientsList;
