import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  Header, Segment, Button, Form, Grid
} from 'semantic-ui-react';
import PropTypes from 'prop-types';

import {
  fetchDish, addDish, updateDish as updateDishAction, deleteDish as deleteDishAction
} from '../../../../redux/actions';
import { getImageSrc } from '../../../../utils/utils';

import DishName from '../DishName';
import DishPrice from '../DishPrice';
import DishPhoto from '../DishPhoto';
import IngredientsList from '../IngredientsList';
import IngredientsListModal from '../IngredientsListModal';

class EditDish extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      photo: null,
      price: 0,
      ingredients: [],
      showModal: false
    };
  }

  componentDidMount() {
    const { getEditDish, dishId } = this.props;
    if (dishId !== 'new') {
      getEditDish(dishId);
    }
  }

  componentDidUpdate(prevProps) {
    const { editDish } = this.props;
    if (editDish) {
      this.updateEditDish(prevProps.editDish, editDish);
    }
  }

  updateEditDish = (prevEditDish, nextEditDish) => {
    if (nextEditDish !== prevEditDish) {
      this.setState({
        name: nextEditDish.name,
        photo: nextEditDish.photo,
        price: nextEditDish.price,
        ingredients: nextEditDish.ingredients
      });
    }
  }

  handleChangeInput = (e, key) => {
    const inputValue = e.target.value;
    this.setState({
      [key]: inputValue
    });
  }

  handleChangeImage = (e) => {
    const photo = e.target.files[0];
    if (photo) {
      this.setState({ photo });
    }
  }

  getFormData = (dish) => {
    const formData = new FormData();
    if (dish.name) {
      formData.append('name', dish.name);
    }
    if (dish.photo) {
      formData.append('photo', dish.photo);
    }
    if (dish.price) {
      formData.append('price', dish.price);
    }
    if (dish.place) {
      formData.append('place', dish.place);
    }
    if (dish.ingredients) {
      for (let i = 0; i < dish.ingredients.length; i += 1) {
        formData.append('ingredients', dish.ingredients[i]);
      }
    }

    return formData;
  }

  handleAddDish = () => {
    const { name, photo, price } = this.state;
    const { placeId, addNewDish, history } = this.props;
    const newDish = {
      name,
      photo,
      price,
      place: placeId,
      ingredients: [5, 3]
    };

    addNewDish(this.getFormData(newDish))
      .then(() => {
        history.push(`/owner/places/${placeId}`);
      });
  }

  getUpdatedFields = (newObj, editObj) => {
    const updatedFields = {};
    const allFields = Object.keys(newObj);
    allFields.forEach((key) => {
      if (newObj[key] !== editObj[key]) {
        updatedFields[key] = newObj[key];
      }
    });

    return updatedFields;
  }

  handleUpdateDish = () => {
    const { name, photo, price } = this.state;
    const {
      updateDish, placeId, editDish, history
    } = this.props;
    const newDish = {
      name,
      photo,
      price,
      place: placeId
    };

    const updatedDish = this.getUpdatedFields(newDish, editDish);
    const formData = this.getFormData(updatedDish);
    updateDish(editDish.id, formData)
      .then(() => {
        history.push(`/owner/places/${placeId}`);
      });
  }

  handleDeleteDish = () => {
    const {
      dishId, deleteDish, history, placeId
    } = this.props;
    deleteDish(dishId, placeId)
      .then(() => {
        history.push(`/owner/places/${placeId}`);
      });
  }

  handleShowModal = () => this.setState({ showModal: true });

  handleCloseModal = () => {
    const { getEditDish, dishId } = this.props;
    this.setState({ showModal: false });
    getEditDish(dishId);
  }

  render() {
    const {
      name, photo, price, ingredients, showModal
    } = this.state;
    const { dishId, loading, error } = this.props;

    if (loading) {
      return <div>loading...</div>;
    }

    if (error) {
      return <div>error</div>;
    }

    return (
      <Segment>
        <Header>{dishId === 'new' ? 'Добавить блюдо ' : 'Редактировать блюдо'}</Header>
        <Form>
          <Grid columns="two">
            <Grid.Row>
              <Grid.Column>
                <DishName
                  dishName={name}
                  onChangeName={this.handleChangeInput}
                />
                <DishPrice
                  dishPrice={typeof price !== 'undefined' ? Number(price) : 0}
                  onChangePrice={this.handleChangeInput}
                />
                <DishPhoto
                  dishImage={getImageSrc(photo)}
                  onChangeImage={this.handleChangeImage}
                />
              </Grid.Column>
              <Grid.Column>
                <IngredientsList
                  ingredients={ingredients}
                  onShowModal={this.handleShowModal}
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                {
                  dishId === 'new'
                    ? (
                      <Form.Group>
                        <Form.Field control={Button} color="teal" onClick={this.handleAddDish}>Добавить</Form.Field>
                      </Form.Group>
                    )
                    : (
                      <Form.Group>
                        <Form.Field control={Button} color="teal" onClick={this.handleUpdateDish}>Сохранить</Form.Field>
                        <Form.Field control={Button} basic color="teal" onClick={this.handleDeleteDish}>Удалить</Form.Field>
                      </Form.Group>
                    )
                }
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Form>
        {
          showModal
            && (
              <IngredientsListModal
                onCloseModal={this.handleCloseModal}
                onUpdateDish={this.handleUpdateDish}
                dishId={dishId}
              />
            )
        }
      </Segment>
    );
  }
}


const mapStateToProps = (state) => ({
  editDish: state.dish.editDish,
  loading: state.dish.loading,
  error: state.dish.error
});

const mapDispatchToProps = {
  getEditDish: fetchDish,
  addNewDish: addDish,
  updateDish: updateDishAction,
  deleteDish: deleteDishAction
};

EditDish.propTypes = {
  editDish: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    photo: PropTypes.string,
    price: PropTypes.number,
    ingredients: PropTypes.arrayOf(PropTypes.object)
  }),
  history: PropTypes.shape({
    push: PropTypes.func
  }).isRequired,
  placeId: PropTypes.string,
  dishId: PropTypes.string,
  error: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  getEditDish: PropTypes.func.isRequired,
  addNewDish: PropTypes.func.isRequired,
  updateDish: PropTypes.func.isRequired,
  deleteDish: PropTypes.func.isRequired
};

EditDish.defaultProps = {
  placeId: '',
  dishId: 0,
  editDish: {
    id: '',
    name: '',
    photo: '',
    price: 0,
    ingredients: []
  },
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(EditDish));
