import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import PropTypes from 'prop-types';

function DishName(props) {
  const { dishName, onChangeName } = props;
  return (
    <Form.Group inline>
      <div className="form-label">
        <label htmlFor="dishName">Название</label>
      </div>
      <div className="form-input">
        <Input id="dishName" value={dishName} onChange={(e) => { onChangeName(e, 'name'); }} />
      </div>
    </Form.Group>
  );
}

DishName.propTypes = {
  dishName: PropTypes.string,
  onChangeName: PropTypes.func.isRequired
};

DishName.defaultProps = {
  dishName: ''
};

export default DishName;
