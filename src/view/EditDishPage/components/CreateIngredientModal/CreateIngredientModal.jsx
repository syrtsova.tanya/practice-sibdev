import React from 'react';
import { Button, Input, Form } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import Modal from '../../../../components/Modal';

function CreateIngredientModal(props) {
  const { onCloseModal, onChangeInput, onAddIngredient } = props;
  return (
    <Modal onCloseModal={onCloseModal} modalTitle="Создать ингредиент">
      <Form>
        <Form.Group inline>
          <div className="form-label">
            <label htmlFor="ingredientName">Название</label>
          </div>
          <div className="form-input">
            <Input id="ingredientName" onChange={(e) => onChangeInput(e, 'name')} />
          </div>
        </Form.Group>
        <Form.Group inline>
          <div className="form-label">
            <label htmlFor="ingredientCalories">Калорийность</label>
          </div>
          <div className="form-input">
            <Input id="ingredientCalories" onChange={(e) => onChangeInput(e, 'calories')} />
          </div>
        </Form.Group>
        <Button color="teal" onClick={onAddIngredient}>Создать</Button>
        <Button basic color="teal" onClick={onCloseModal}>Отмена</Button>
      </Form>
    </Modal>
  );
}

CreateIngredientModal.propTypes = {
  onCloseModal: PropTypes.func.isRequired,
  onChangeInput: PropTypes.func.isRequired,
  onAddIngredient: PropTypes.func.isRequired
};

export default CreateIngredientModal;
