import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  Button, Checkbox,
} from 'semantic-ui-react';

import {
  fetchIngredients as fetchIngredientsAction,
  updateDish as updateDishAction,
  addIngredient as addIngredientAction
} from '../../../../redux/actions';
import Modal from '../../../../components/Modal';
import CreateIngredientModal from '../CreateIngredientModal';
import './ingredientsListModal.scss';

class IngredientsListModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
      checkedIngredients: [],
      newIngredient: {
        name: '',
        calories: 0
      }
    };
  }

  componentDidMount() {
    const { fetchIngredients } = this.props;
    fetchIngredients();
  }

  handleChangeInput = (e, key) => {
    const inputValue = e.target.value;
    const { newIngredient } = this.state;
    this.setState({
      newIngredient: {
        ...newIngredient,
        [key]: inputValue
      }
    });
  }

  handleCheckIngredient = (id) => {
    const { checkedIngredients } = this.state;
    let ingredients = checkedIngredients.slice();
    if (!checkedIngredients.find((el) => el === id)) {
      ingredients.push(id);
    } else {
      ingredients = checkedIngredients.filter((el) => el !== id);
    }
    this.setState({ checkedIngredients: ingredients });
  }

  handleShowModal = () => this.setState({ showModal: true });

  handleCloseModal = () => this.setState({ showModal: false });

  getFormData = (ingredients) => {
    const formData = new FormData();
    for (let i = 0; i < ingredients.length; i += 1) {
      formData.append('ingredients', ingredients[i]);
    }
    return formData;
  }

  handleUpdateIngredients = () => {
    const { checkedIngredients } = this.state;
    const {
      updateDish, dishId, onCloseModal, fetchIngredients
    } = this.props;

    updateDish(dishId, this.getFormData(checkedIngredients))
      .then(() => {
        fetchIngredients();
        onCloseModal();
      });
  };

  handleAddIngredient = () => {
    const { addIngredient } = this.props;
    const { newIngredient } = this.state;
    const formData = new FormData();

    formData.append('name', newIngredient.name);
    formData.append('calories', newIngredient.calories);

    addIngredient(formData)
      .then(() => {
        this.handleCloseModal();
      });
  };

  render() {
    const {
      onCloseModal, loading, error, ingredientsList
    } = this.props;
    const { showModal } = this.state;

    return (
      <Modal onCloseModal={onCloseModal} modalTitle="Добавить ингредиенты">
        {loading && 'Loading...'}
        {error && 'Error'}
        <div className="ingredients-list">
          {
            ingredientsList && ingredientsList.map((ingredient) => (
              <Checkbox
                className="ingredients-list__item"
                key={ingredient.id}
                label={ingredient.name}
                onChange={() => { this.handleCheckIngredient(ingredient.id); }}
              />
            ))
          }
        </div>
        <div className="modal__footer">
          <Button basic color="teal" onClick={this.handleShowModal}>Создать новый ингредиент</Button>
          <Button basic color="teal" onClick={this.handleUpdateIngredients}>Добавить выбранные ингредиенты</Button>
        </div>
        {
          showModal
          && (
            <CreateIngredientModal
              onCloseModal={this.handleCloseModal}
              onChangeInput={this.handleChangeInput}
              onAddIngredient={this.handleAddIngredient}
            />
          )
        }
      </Modal>
    );
  }
}

const mapStateToProps = (state) => ({
  ingredientsList: state.ingredients.ingredientsList,
  loading: state.ingredients.loading,
  error: state.ingredients.error
});

const mapDispatchToProps = {
  fetchIngredients: fetchIngredientsAction,
  updateDish: updateDishAction,
  addIngredient: addIngredientAction
};

IngredientsListModal.propTypes = {
  onCloseModal: PropTypes.func.isRequired,
  fetchIngredients: PropTypes.func.isRequired,
  updateDish: PropTypes.func.isRequired,
  addIngredient: PropTypes.func.isRequired,
  dishId: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
  ingredientsList: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string
  }))
};

IngredientsListModal.defaultProps = {
  ingredientsList: []
};

export default connect(mapStateToProps, mapDispatchToProps)(IngredientsListModal);
