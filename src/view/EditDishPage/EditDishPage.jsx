import React from 'react';
import { Container } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import EditDish from './components/EditDish';

function EditDishPage(props) {
  const { placeId, dishId } = props;
  return (
    <Container>
      <EditDish placeId={placeId} dishId={dishId} />
    </Container>
  );
}

EditDishPage.propTypes = {
  placeId: PropTypes.string.isRequired,
  dishId: PropTypes.string.isRequired
};

export default EditDishPage;
